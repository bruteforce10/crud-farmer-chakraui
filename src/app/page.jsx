"use client";
import {
  Box,
  Button,
  ChakraProvider,
  Container,
  FormControl,
  FormLabel,
  HStack,
  Heading,
  Input,
  Skeleton,
  Spinner,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Thead,
  Tr,
  VStack,
  useToast,
} from "@chakra-ui/react";
import { useFetchProducts } from "@/features/product/useFetchProduct";
import { useFormik } from "formik";
import { useMutation } from "@tanstack/react-query";
import { axiosInstance } from "@/lib/axios";
import {
  useCreateProduct,
  useEditProduct,
  useDeleteProduct,
  useFetchProduct,
} from "@/features/product";

export default function Home() {
  const toast = useToast();
  const {
    data: products,
    isLoading: productsIsLoading,
    refetch: refetchProducts,
  } = useFetchProducts({
    onError: (error) => {
      toast({
        title: error.message,
        status: "error",
      });
    },
  });

  const { mutate: createProduct, isLoading: createProductIsLoading } =
    useCreateProduct({
      onSuccess: () => {
        refetchProducts();
      },
    });

  const formik = useFormik({
    initialValues: {
      id: "",
      name: "",
      price: "",
      description: "",
      image: "",
    },
    onSubmit: (values, actions) => {
      if (values.id) {
        editProduct(values);
        toast({
          title: "Product Edited",
          status: "success",
        });
      } else {
        createProduct(values);
        toast({
          title: "Product Added",
          status: "success",
        });
      }

      actions.resetForm();
    },
  });

  const { mutate: editProduct, isLoading: editProductIsLoading } =
    useEditProduct({
      onSuccess: () => {
        refetchProducts();
      },
    });

  const { mutate: deleteProduct } = useDeleteProduct({
    onSuccess: () => {
      refetchProducts();
    },
  });

  const handleFormInput = (event) => {
    formik.setFieldValue(event.target.name, event.target.value);
  };

  const confirmationDelete = (id) => {
    const shouldDelete = confirm("Are you sure?");

    if (shouldDelete) {
      deleteProduct(id);
      toast({
        title: "Product Deleted",
        status: "info",
      });
    }
  };

  const onEdit = (productId) => {
    formik.setFieldValue("id", productId.id);
    formik.setFieldValue("name", productId.name);
    formik.setFieldValue("price", productId.price);
    formik.setFieldValue("description", productId.description);
    formik.setFieldValue("image", productId.image);
  };

  const renderProducts = () => {
    return products?.data.map((product) => {
      return (
        <Tr key={product.id}>
          <Td>{product.id}</Td>
          <Td>{product.name}</Td>
          <Td>{product.price}</Td>
          <Td>{product.description}</Td>
          <Td>
            <HStack spacing={4}>
              <Button onClick={() => onEdit(product)} colorScheme="cyan">
                Edit
              </Button>
              <Button
                onClick={() => confirmationDelete(product.id)}
                colorScheme="red"
              >
                Delete
              </Button>
            </HStack>
          </Td>
        </Tr>
      );
    });
  };

  return (
    <Box mx={"40px"}>
      <ChakraProvider>
        <Container>
          <Heading>Hello</Heading>
        </Container>
        <Table mb={"6"}>
          <Thead>
            <Tr>
              <Td>Id</Td>
              <Td>Name</Td>
              <Td>Price</Td>
              <Td>Descriptions</Td>
              <Td colSpan={2}>Action</Td>
            </Tr>
          </Thead>
          <Tbody>
            {renderProducts()}
            {productsIsLoading && (
              <Td colSpan={5}>
                <Stack>
                  <Skeleton height="20px" />
                  <Skeleton height="20px" />
                  <Skeleton height="20px" />
                  <Skeleton height="20px" />
                </Stack>
              </Td>
            )}
          </Tbody>
        </Table>
        <form onSubmit={formik.handleSubmit}>
          <VStack spacing={"3"}>
            <FormControl>
              <FormLabel>Product Id</FormLabel>
              <Input
                onChange={handleFormInput}
                name="id"
                value={formik.values.id}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Product Label</FormLabel>
              <Input
                onChange={handleFormInput}
                name="name"
                value={formik.values.name}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Price</FormLabel>
              <Input
                onChange={handleFormInput}
                name="price"
                value={formik.values.price}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Description</FormLabel>
              <Input
                onChange={handleFormInput}
                name="description"
                value={formik.values.description}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Image</FormLabel>
              <Input
                onChange={handleFormInput}
                name="image"
                value={formik.values.image}
              />
            </FormControl>
            {createProductIsLoading || editProductIsLoading ? (
              <Spinner />
            ) : (
              <Button type="submit">Submit Product</Button>
            )}
          </VStack>
        </form>
      </ChakraProvider>
    </Box>
  );
}
