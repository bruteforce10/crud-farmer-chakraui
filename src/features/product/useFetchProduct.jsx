import { useEffect, useState } from "react";
import { axiosInstance } from "@/lib/axios";
import { useQuery, useQueryClient } from "@tanstack/react-query";

export const useFetchProducts = ({ onError }) => {
  // const [products, setProducts] = useState([]);
  // const [isLoading, setIsLoading] = useState(false);

  // const fetchProducts = async () => {
  //   setIsLoading(true);
  //   try {
  //     setTimeout(async () => {
  //       const productsResponse = await axiosInstance.get("/products");
  //       setProducts(productsResponse.data);
  //       setIsLoading(false);
  //     }, 1000);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // useEffect(() => {
  //   fetchProducts();
  // }, []);

  return useQuery({
    queryFn: async () => {
      const productsResponse = await axiosInstance.get("/products");
      return productsResponse;
    },
    queryKey: ["fetch.products"],
    onError,
  });
};
